﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Monstruo : MonoBehaviour {

    #region movimiento

    /// <summary>
    /// Velocidad de movimiento del Monstruo al andar
    /// </summary>
    public float walkSpeed = 2f;

    /// <summary>
    /// Los objetos de que Layers impiden su paso
    /// </summary>
    public LayerMask obstacleMask;

    /// <summary>
    /// Punto medio frontal, usado para algunas colisiones
    /// </summary>
    public Transform middleFront;
    #endregion movimiento

    #region Posesion

    /// <summary>
    /// Indica si el Monstruo puede poseer 
    /// </summary>
    public bool canPossess = false;

    /// <summary>
    /// Indica si está poseyendo algo actualmente
    /// </summary>
    public bool isPossessing = false;

    /// <summary>
    /// Referencia al obejeto actualmente poseido
    /// </summary>
    protected ObjetoPoseible objetoPoseido;

    /// <summary>
    /// Señala al objeto poseible actualmente seleccionado como objetivo de la posesión
    /// </summary>
    public ObjetoPoseible ObjetoPoseibleCercano { get; set; }

    /// <summary>
    /// En todo momento contiene los objetos poseibles dentro del radio de posesión
    /// </summary>
    protected List<ObjetoPoseible> objetosPoseiblesCercanos = new List<ObjetoPoseible>();

    /// <summary>
    /// Distancia a la que puede poseer  objetos
    /// </summary>    
    public float PossessionDistance = 0.3f;

    /// <summary>
    /// El radio inicial de su collider. Se le suma la PossessionDistance para calcular el collider empleado para la posesión.
    /// </summary>
    public float InitialColliderRadius;

    #endregion

    #region Referencias a componentes

    protected Animator animator;
    protected Rigidbody2D rb2D;
    protected Transform _transform;
    protected CircleCollider2D _collider;
    protected SpriteRenderer _renderer;

    #endregion

    #region Miedo
    
    /// <summary>
    /// El poder de miedo recolectado por el Monstruo
    /// </summary>
    [Range(0, 100)]
    protected int poderDeMiedo;
    public int PoderDeMiedo
    {
        get
        {
            return poderDeMiedo;
        }
        set
        {
            poderDeMiedo = value;
        }
    }

    /// <summary>
    /// El Horror que provoca el Monstruo
    /// </summary>
    public int NivelDeHorror;

    #endregion

    #region Aspecto
    public Color colorActual;
    public Color colorInicial;
    public float ColorChangeSpeed = 3f;

    #endregion


    #region Metodos Unity
    /// <summary>
    /// Inicializa las referencias a componentes.
    /// Guarda el color inicial.
    /// Actualiza el radio del collider según PossessionDistance
    /// </summary>
    void Start()
    {
        animator = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        _transform = transform;
        _collider = GetComponent<CircleCollider2D>();
        _renderer = GetComponent<SpriteRenderer>();

        _renderer.color = colorInicial;
        colorActual = colorInicial;
        _collider.radius = InitialColliderRadius + PossessionDistance; 
    }

    #endregion

    #region Metodos Posesion

    /// <summary>
    /// Posee ObjetoPoseibleCercano, si puede
    /// </summary>
    public void possess()
    {
        if (canPossess && !isPossessing)   
        {
            _renderer.enabled = false;
            objetoPoseido = ObjetoPoseibleCercano;
            objetoPoseido.SetPossession(true);
            isPossessing = true;
        }
    }
    
    /// <summary>
    /// Deja de poseer objetoPoseido
    /// </summary>
    public void unpossess()
    {
        if (isPossessing)
        {
            objetoPoseido.SetPossession(false);
            objetoPoseido = null;
            _renderer.enabled = true;
            isPossessing = false;
        }
    }


    /// <summary>
    /// Cambia el color del monstruo por la posesion
    /// </summary>
    /// <param name="poseyendo">Cierto si esta entrando a poseer, falso si está desposeyendo</param>
    public void CambiaColorPorPosesion(bool poseyendo)
    {
        if (poseyendo)
            CambiaColor(ObjetoPoseibleCercano.MainColor);
        else
            CambiaColor(colorInicial);
    }

    /// <summary>
    /// Añade un objeto a la lista de objetos poseibles cercanos
    /// </summary>
    /// <param name="objeto">El objeto poseible que está al alcance ahora</param>
    public void AddObjetoPoseibleCercano(ObjetoPoseible objeto)
    {
        if (!objetosPoseiblesCercanos.Contains(objeto))
            objetosPoseiblesCercanos.Add(objeto);
        if (ObjetoPoseibleCercano != null)
            ObjetoPoseibleCercano.SetPlayerFocus(false);
        ObjetoPoseibleCercano = objeto;
        ObjetoPoseibleCercano.SetPlayerFocus(true);
        canPossess = true;
    }


    /// <summary>
    /// Elimina un objeto de la lista de objetos poseibles cercanos
    /// </summary>
    /// <param name="objeto">El objeto poseible que ya no está al alcance</param>
    public void RemoveObjetoPoseibleCercano(ObjetoPoseible objeto)
    {
        if (!objetosPoseiblesCercanos.Contains(objeto))
            return;

        objetosPoseiblesCercanos.Remove(objeto);
        if (ObjetoPoseibleCercano.Equals(objeto))
        {
            if (objetosPoseiblesCercanos.Count > 0)
                ObjetoPoseibleCercano = objetosPoseiblesCercanos[0];
            else
            {
                ObjetoPoseibleCercano = null;
                canPossess = false;
            }
        }
    }

    #endregion

    #region Metodos de Aspecto

    /// <summary>
    /// Cambia el color del monstruo
    /// </summary>
    /// <param name="destino">Color al que cambiar</param>
    public void CambiaColor(Color destino)
    {
        StartCoroutine(cambioColor(destino));
    }

    /// <summary>
    /// Corrutina para el cambio de color
    /// </summary>
    /// <param name="destino">Color al que cambiar</param>
    private IEnumerator cambioColor(Color destino)
    {
        while (_renderer.color != destino)
        {
            _renderer.color = Color.Lerp(_renderer.color, destino, Time.deltaTime * ColorChangeSpeed);
            yield return null;
        }
        _renderer.color = colorActual = destino;
    }

    #endregion


}
