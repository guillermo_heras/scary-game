﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MensajeComunicado
{
    public Intereses Interes { get; set; }
    public Investigable Objeto { get; set; }
    public int Intensidad { get; set; }
    public string Mensaje { get; set; }
    public bool Comunicando { get; set; }
    public float TimeInicialComunicacion { get; set; }

    public MensajeComunicado()
    {
        Comunicando = false;
    }

    public MensajeComunicado(Intereses interes)
        :this()
    {
        Interes = interes;
    }

    public MensajeComunicado(Intereses interes, int intensidad)
    : this(interes)
    {
        Intensidad = intensidad;
    }

    public MensajeComunicado(Intereses interes, Investigable objeto)
    : this(interes)
    {
        Objeto = objeto;
    }

    public MensajeComunicado(Intereses interes, Investigable objeto, int intensidad)
    : this(interes, objeto)
    {
        Intensidad = intensidad;
    }

    public bool IndicaObjeto()
    {
        return Objeto != null;
    }
}

public class Comunicador : MonoBehaviour {

    /// <summary>
    /// Sprite que mostrará el bocadillo visual de la comunicación
    /// </summary>
    public SpriteRenderer spriteBocadillo;

    /// <summary>
    /// Sprite que mostrará el icono del interés indicado por la comunicación
    /// </summary>
    public SpriteRenderer spriteInteres;

    /// <summary>
    /// Sprite que mostrará el objeto indicado por la comunicación
    /// </summary>
    public SpriteRenderer spriteObjeto;

    /// <summary>
    /// A falta de otra entrada, el tiempo que durará cada comunicación
    /// </summary>
    public float tiempoStandardComunicacion = 2f;

    /// <summary>
    /// Indica si ya se está comunicando un mensaje
    /// </summary>
    private bool comunicando = false;

    /// <summary>
    /// Almacena el mensaje que se está comunicando en el momento actual
    /// </summary>
    private MensajeComunicado mensajeActual;

    private Iconos iconos;

	// Use this for initialization
	void Start () {
        iconos = GameObject.FindObjectOfType<Iconos>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (comunicando)
        {
            if (Time.time >= mensajeActual.TimeInicialComunicacion + tiempoStandardComunicacion)
            {
                CortaComunicacion();
            }
        }
	}

    public void Comunica(MensajeComunicado mensaje)
    {
        if (comunicando)
        {

        }
        else
        {
            comunicando = true;
            spriteInteres.sprite = iconos.GetIconoParaInteres(mensaje.Interes);
            spriteObjeto.sprite = mensaje.Objeto.GetSprite();                
            spriteBocadillo.enabled = true;
            spriteInteres.enabled = true;
            spriteObjeto.enabled = true;
            mensaje.Comunicando = true;
            mensaje.TimeInicialComunicacion = Time.time;
            mensajeActual = mensaje;
        }
    }

    public void CortaComunicacion()
    {
        spriteInteres.sprite = null;
        spriteObjeto.sprite = null;
        spriteBocadillo.enabled = false;
        spriteInteres.enabled = false;
        spriteObjeto.enabled = false;
        mensajeActual = null;
        comunicando = false;
    }
}
