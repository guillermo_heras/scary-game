﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MonstruoTrepador : Player {

	public float climbSpeed = 2.0f;

    public Transform middleLow;
    public Transform middleHigh;

	public override void Update() {
		base.Update ();
        
		int vertical = 0;
		float climbingMovement;

		vertical = (int)Input.GetAxisRaw ("Vertical");

        if (vertical != 0)
        {
            climbingMovement = vertical * climbSpeed * Time.deltaTime;

            Vector2 movement = new Vector2(0, climbingMovement);

            RaycastHit2D rayCast;

            if (vertical > 0)
                rayCast = Physics2D.Raycast(middleHigh.position, movement, movement.magnitude, obstacleMask);
            else
                rayCast = Physics2D.Raycast(middleLow.position, movement, movement.magnitude, obstacleMask);            

            if (rayCast.collider == null)
            {
                Vector2 newPosition = new Vector2(_transform.position.x, _transform.position.y + climbingMovement);
                animator.SetBool("Climbing", true);
                rb2D.MovePosition(newPosition);
            }
            else if (rayCast.collider.tag == "Floor")
            {
                animator.SetBool("Climbing", false);
            }
            else
            {
                animator.SetBool("Climbing", true);

            }
        }
	}

}
