﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameMaster : MonoBehaviour {

    private Player player;
    public Investigable modeloInvestigable;

    private Investigable[] poolInvestigables;
    private bool[] investigablesLibres;

    public int tamañoInicialPoolInvestigables = 10;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        player.PoderDeMiedo = 10;

        poolInvestigables = new Investigable[tamañoInicialPoolInvestigables];
        for (int i = 0; i < tamañoInicialPoolInvestigables; i++)
            poolInvestigables[i] = Instantiate(modeloInvestigable);
	}
	
    private bool isLibre(int i)
    {
        return investigablesLibres[i];
    }

    public Investigable getNewInvestigable()
    {
        int i = 0;
        while (!isLibre(i))
            i++;

        if (i >= poolInvestigables.Length)
            poolInvestigables.
    }

}
