﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class IconoParaInteres
{
    public Intereses interes;
    public Sprite icono;
}

public class Iconos : MonoBehaviour {

    public IconoParaInteres[] iconosParaIntereses;

    private Dictionary<Intereses, Sprite> iconos = new Dictionary<Intereses, Sprite>();

	// Use this for initialization
	void Start () {
        foreach (IconoParaInteres ipi in iconosParaIntereses)
        {
            iconos.Add(ipi.interes, ipi.icono);
        }
	}
	
    public Sprite GetIconoParaInteres(Intereses interes)
    {
        return iconos[interes];
    }
}
