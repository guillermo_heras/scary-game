﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Asustable : MonoBehaviour
{

    public Animator animator;

    /// <summary>
    /// El Nivel de Miedo indica cuánto miedo tiene en un momento dado el Humano. Se mide de 0 a 100. 
    /// Al aumentar hace al humano avanzar de un Nivel de Horror a otro.
    /// </summary>
    [Range(0, 100)]
    [SerializeField]
    [HideInInspector]
    private int nivelDeMiedo;
    public int NivelDeMiedo
    {
        get
        {
            return nivelDeMiedo;
        }

        set
        {
            nivelDeMiedo = value;
            UpdateMiedo();
        }
    }

    /// <summary>
    /// El Nivel de Horror va de 0 a 4. Indica en que estado de terror se encuentra el Humano.
    /// Si el Nivel de Miedo es 0, el Nivel de Horror es 0.
    /// Si el Nivel de Miedo es mayor que 0, el Nivel de Horror es al menos 1.
    /// </summary>
    public int NivelDeHorror
    {
        get
        {
            if (nivelDeMiedo == 0)
            {
                return 0;
            }
            else if (nivelDeMiedo < InicioMiedo2)
            {
                return 1;
            }
            else if (nivelDeMiedo < InicioMiedo3)
            {
                return 2;
            }
            else if (nivelDeMiedo < InicioMiedo4)
            {
                return 3;
            }
            else
            {
                return 4;
            }
        }
    }

    /// <summary>
    /// inicioMiedo2 indica en qué valor de Nivel de Miedo el Humano entra al Nivel de Horror 2.
    /// </summary>
    [Range(0, 100)]
    [SerializeField]
    private int inicioMiedo2;
    public int InicioMiedo2
    {
        get
        {
            return inicioMiedo2;
        }

        set
        {
            inicioMiedo2 = value;
        }
    }

    /// <summary>
    /// inicioMiedo3 indica en qué valor de Nivel de Miedo el Humano entra al Nivel de Horror 3.
    /// </summary>
    [Range(0, 100)]
    [SerializeField]
    private int inicioMiedo3;
    public int InicioMiedo3
    {
        get
        {
            return inicioMiedo3;
        }

        set
        {
            inicioMiedo3 = value;
        }
    }

    /// <summary>
    /// inicioMiedo4 indica en qué valor de Nivel de Miedo el Humano entra al Nivel de Horror 4.
    /// </summary>
    [Range(0, 100)]
    [SerializeField]
    private int inicioMiedo4;
    public int InicioMiedo4
    {
        get
        {
            return inicioMiedo4;
        }

        set
        {
            inicioMiedo4 = value;
        }
    }

    /// <summary>
    /// Temor indica cuánto le afecta el Miedo.
    /// </summary>
    [Range(0, 100)]
    [SerializeField]
    private int temor;
    /// <summary>
    /// Temor indica cuánto le afecta el Miedo.
    /// </summary>
    public int Temor
    {
        get
        {
            return temor;
        }

        set
        {
            temor = value;
        }
    }
    
    /// <summary>
    /// Valentia indica cuánto soporta el Terror. Es igual a 100-Temor
    /// </summary>
    public int Valentia
    {
        get
        {
            return 100 - Temor;
        }
    }

    /// <summary>
    /// Símbolo que representa el nivel de Horror 1
    /// </summary>
    public SpriteRenderer SimboloDeHorrorNivel1;

    /// <summary>
    /// Símbolo que representa el nivel de Horror 2
    /// </summary>
    public SpriteRenderer SimboloDeHorrorNivel2;

    /// <summary>
    /// Símbolo que representa el nivel de Horror 3
    /// </summary>
    public SpriteRenderer SimboloDeHorrorNivel3;

    /// <summary>
    /// Símbolo que representa el nivel de Horror 4
    /// </summary>
    public SpriteRenderer SimboloDeHorrorNivel4;

    /// <summary>
    /// Multiplicador a la velocidad de las animaciones con Nivel de Horror 0.
    /// </summary>
    public float velocidadDeAnimacionConHorror0 = 1.0f;

    /// <summary>
    /// Multiplicador a la velocidad de las animaciones con Nivel de Horror 1.
    /// </summary>
    public float velocidadDeAnimacionConHorror1;

    /// <summary>
    /// Multiplicador a la velocidad de las animaciones con Nivel de Horror 2.
    /// </summary>
    public float velocidadDeAnimacionConHorror2;

    /// <summary>
    /// Multiplicador a la velocidad de las animaciones con Nivel de Horror 3.
    /// </summary>
    public float velocidadDeAnimacionConHorror3;

    /// <summary>
    /// Multiplicador a la velocidad de las animaciones con Nivel de Horror 4.
    /// </summary>
    public float velocidadDeAnimacionConHorror4;

    /// <summary>
    /// Lista de objetos que han asustado al Asustable
    /// </summary>
    public List<Investigable> ObjetosConocidosAterradores;

    /// <summary>
    /// Actualiza el efecto del Nivel de Horror en el juego: muestra los Simbolos de Horror que se corresponden, 
    /// y aplica el multiplicador a la velocidad de animaciones que corresponda.
    /// </summary>
    public void UpdateMiedo()
    {
        Debug.Log("Actualizamos el miedo");
        if (NivelDeHorror == 0)
        {
            Debug.Log("Horror 0");
            SimboloDeHorrorNivel1.enabled = false;
            SimboloDeHorrorNivel2.enabled = false;
            SimboloDeHorrorNivel3.enabled = false;
            SimboloDeHorrorNivel4.enabled = false;
            animator.speed = velocidadDeAnimacionConHorror0;
        }
        else if (NivelDeHorror == 1)
        {
            Debug.Log("Horror 1");
            SimboloDeHorrorNivel1.enabled = true;
            SimboloDeHorrorNivel2.enabled = false;
            SimboloDeHorrorNivel3.enabled = false;
            SimboloDeHorrorNivel4.enabled = false;
            animator.speed = velocidadDeAnimacionConHorror1;
        }
        else if (NivelDeHorror == 2)
        {
            Debug.Log("Horror 2");

            SimboloDeHorrorNivel1.enabled = true;
            SimboloDeHorrorNivel2.enabled = true;
            SimboloDeHorrorNivel3.enabled = false;
            SimboloDeHorrorNivel4.enabled = false;
            animator.speed = velocidadDeAnimacionConHorror2;
        }
        else if (NivelDeHorror == 3)
        {
            Debug.Log("Horror 3");

            SimboloDeHorrorNivel1.enabled = true;
            SimboloDeHorrorNivel2.enabled = true;
            SimboloDeHorrorNivel3.enabled = true;
            SimboloDeHorrorNivel4.enabled = false;
            animator.speed = velocidadDeAnimacionConHorror3;
        }
        else
        {
            Debug.Log("Horror 4");

            SimboloDeHorrorNivel1.enabled = true;
            SimboloDeHorrorNivel2.enabled = true;
            SimboloDeHorrorNivel3.enabled = true;
            SimboloDeHorrorNivel4.enabled = true;
            animator.speed = velocidadDeAnimacionConHorror4;
        }
        Debug.Log("Horror ??");

    }

    /// <summary>
    /// Lanza un Trigger de Susto al animator. Aumenta el nivel de miedo.
    /// </summary>
    /// <param name="nivelDeSusto">El nivel del susto sufrido.</param>
    public void Susto(int nivelDeSusto)
    {
        NivelDeMiedo += nivelDeSusto;
        animator.SetTrigger("Susto");
    }

    /// <summary>
    /// Calcula el Valor de Terror de un Investigable según la situación actual del Asustable.
    /// El Valor de Terror es igual al Horror del Investigable, +10% por cada Nivel de Horror actual del Asustable. Máximo 100.
    /// </summary>
    /// <param name="investigable">El investigable cuyo valor de Terror se quiere calcular.</param>
    /// <returns>El Valor de Terror actual para el Asustable del Investigable indicado.</returns>
    public int ValorDeTerror(Investigable investigable)
    {
        int terror = investigable.Horror + (int)(NivelDeHorror * 0.1f * investigable.Horror);
        return (terror > 100) ? 100 : terror;
    }

    /// <summary>
    /// Comprueba si un Investigable da un Susto al Asustable.
    /// </summary>
    /// <param name="investigable">El Investigable que puede asustar al Asustable.</param>
    public bool ComprobarSusto(Investigable investigable)
    {
        int valorDeTerror = ValorDeTerror(investigable);

        bool susto = valorDeTerror >= Valentia;

        if (susto)
        {
            int nivelDeSusto = valorDeTerror - Valentia;
            Debug.Log("Me asusta " + investigable.name + " con una fuerza de " + nivelDeSusto);
            ObjetosConocidosAterradores.Add(investigable);
            Susto(nivelDeSusto);
        }
        else
        {
            NivelDeMiedo += Mathf.FloorToInt(valorDeTerror * 0.1f);
        }

        return susto;
    }

    /// <summary>
    /// Inicializa las referencias a componentes.
    /// </summary>
    public virtual void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }
}

