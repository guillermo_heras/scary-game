﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Player : Monstruo {

    public Text poderDeMiedoUI;

    new public int PoderDeMiedo
    {
        get
        {
            return poderDeMiedo;
        }
        set
        {
            poderDeMiedo = value;
            poderDeMiedoUI.text = poderDeMiedo.ToString();
        }
    }

    private bool hayColisiones(float xMove)
    {
        Vector2 movement = new Vector2(xMove, 0);
        RaycastHit2D rayCast = Physics2D.Raycast(middleFront.position, movement, movement.magnitude, obstacleMask);
        return rayCast.collider != null;
    }

    private void stopAnimatorMovement()
    {
        animator.SetFloat("Speed", 0);
        animator.SetTrigger("Stop");
    }

    public virtual void Update()
    {
        float horizontal = 0;

        if (!isPossessing)
        {
            //Velocidad de movimiento horizontal
            horizontal = Input.GetAxisRaw("Horizontal");
            
            //Se da la vuelta al sprite si se avanza hacia -x
            _renderer.flipX = horizontal >= 0;

            //Si hay entrada en x (se quiere mover)...
            if (horizontal != 0)
            {
                float xMove = horizontal * walkSpeed * Time.deltaTime;

                if (hayColisiones(xMove))
                {
                    stopAnimatorMovement();
                }
                else
                {
                    Vector2 newPosition = new Vector2(_transform.position.x + xMove, _transform.position.y);
                    animator.SetFloat("Speed", xMove);
                    rb2D.MovePosition(newPosition);
                }
            }
            else
            {
                stopAnimatorMovement();
            }

            //Comprueba si posee
            if (canPossess)
            {
                if (Input.GetButtonDown(GameInput.Possess))
                {
                    animator.SetTrigger("Possess");
                }
            }

            if (objetosPoseiblesCercanos.Count > 0)
            {
                if (Input.GetButtonDown(GameInput.CiclePossessables))
                {
                    float direction = Input.GetAxis(GameInput.CiclePossessables);

                    int currentIndex = objetosPoseiblesCercanos.IndexOf(ObjetoPoseibleCercano);
                    int nextIndex = (currentIndex == objetosPoseiblesCercanos.Count - 1) ? 0 : currentIndex + 1;
                    int previousIndex = (currentIndex == 0) ? objetosPoseiblesCercanos.Count - 1 : currentIndex - 1;

                    if (ObjetoPoseibleCercano != null)
                        ObjetoPoseibleCercano.SetPlayerFocus(false);
                    ObjetoPoseibleCercano = (direction > 0) ? objetosPoseiblesCercanos[nextIndex] : objetosPoseiblesCercanos[previousIndex];
                    ObjetoPoseibleCercano.SetPlayerFocus(true);

                }
            }
        }
        else
        {
            if (Input.GetButtonDown(GameInput.Unpossess))
            {
                animator.SetTrigger("Possess");
            }
            else if (Input.GetButtonDown(GameInput.CauseEffect))
            {
                EfectoPoseer();
                
            }
        }
    }

    public void EfectoPoseer()
    {
        objetoPoseido.EfectoPosesion();

    }

}
