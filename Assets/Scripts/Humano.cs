﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Intereses de los humanos. Base de sus impulsos.
/// Ocio, Trabajo, Misterio, Interes, Miedo
/// </summary>
public enum Intereses
{
    Ocio, Trabajo, Misterio, Interes, Miedo
}

/// <summary>
/// Indica cuanto trabajo debe realizar un humano en un lugar de trabajo concreto.
/// </summary>
[System.Serializable]
public class TrabajoPorHacer
{
    public int cantidad;
    public Investigable lugar;
    
    /// <summary>
    /// Devuelve la productividad posible de este trabajo por hacer. 
    /// Depende de la cantidad de trabajo que incluye, y del valor de Trabajo del lugar.
    /// </summary>
    /// <returns>Un valor de 0 a 100 con la productividad</returns>
    public int Productividad()
    {
        int productividad = Mathf.FloorToInt(cantidad * lugar.Trabajo / 100);
        return (productividad > 100) ? 100 : productividad;
    }
}

/// <summary>
/// Define a los humanos básicos del juego.
/// </summary>
public class Humano : Asustable {

    #region Campos y Propiedades
    #region Impulsos

    /// <summary>
    /// Impulsos del Humano.    
    /// </summary>

    /// <summary>
    /// Curiosidad indica cuánto le afecta el Misterio.
    /// </summary>
    [SerializeField]
    [Range(0, 100)]
    private int curiosidad;
    /// <summary>
    /// Curiosidad indica cuánto le afecta el Misterio.
    /// </summary>
    public int Curiosidad
    {
        get
        {
            return curiosidad;
        }

        set
        {
            curiosidad = value;
        }
    }

    /// <summary>
    /// Ocio indica cuánto le gusta divertirse.
    /// </summary>
    [SerializeField]
    [Range(0, 100)]
    private int ocio;
    /// <summary>
    /// Ocio indica cuánto le gusta divertirse.
    /// </summary>
    public int Ocio
    {
        get
        {
            return ocio;
        }

        set
        {
            ocio = value;
        }
    }

    /// <summary>
    /// Aguante indica cuánto aguanta trabajando de seguido.
    /// </summary>
    [SerializeField]
    [Range(0, 100)]
    private int aguante;
    /// <summary>
    /// Aguante indica cuánto aguanta trabajando de seguido.
    /// </summary>
    public int Aguante
    {
        get
        {
            return aguante;
        }

        set
        {
            aguante = value < 0 ? 0 : value;
        }
    }

    /// <summary>
    /// Pacifismo indica cuánto intenta evitar la violencia.
    /// </summary>
    [SerializeField]
    [Range(0, 100)]
    private int pacifismo;
    /// <summary>
    /// Pacifismo indica cuánto intenta evitar la violencia.
    /// </summary>
    public int Pacifismo
    {
        get
        {
            return pacifismo;
        }

        set
        {
            pacifismo = value;
        }
    }

    /// <summary>
    /// Lista de objetos que han llamado la atención del Humano (han superado su Curiosidad)
    /// </summary>
    public List<Investigable> ObjetosConocidosInteresantes;

    /// <summary>
    /// objeto al que se quiere dirigir ahora mismo el Humano.
    /// </summary>
    private Investigable destinoActual;
    
    /// <summary>
    /// objeto al que se quiere dirigir ahora mismo el Humano.
    /// </summary>
    public Investigable DestinoActual
    {
        get
        {
                return destinoActual;
        }

        set
        {
            destinoActual = value;
        }
    }

    /// <summary>
    /// Lugar de trabajo actual del Humano.
    /// </summary>
    private Investigable trabajoActual;

    /// <summary>
    /// Lugar de ocio actual del Humano.
    /// </summary>
    private Investigable ocioActual;
    
    /// <summary>
    /// Lugar de trabajo actual del Humano.
    /// </summary>
    public Investigable TrabajoActual
    {
        get
        {
            return trabajoActual;
        }

        set
        {
            trabajoActual = value;
        }
    }

    /// <summary>
    /// Lugares de trabajo que el humano conoce y donde tiene trabajo que hacer.
    /// </summary>
    public TrabajoPorHacer[] LugaresDeTrabajoConocidos = new TrabajoPorHacer[0];
    
    /// <summary>
    /// Lugares de ocio que el humano conoce.
    /// </summary>
    public Investigable[] LugaresDeOcioConocidos = new Investigable[0];

    /// <summary>
    /// El último momento en que se ejecutó por completo la actividad actual
    /// </summary>
    private float ultimoTiempoActualizacionActividad;

    /// <summary>
    /// La dedicación actual 
    /// </summary>
    private TrabajoPorHacer trabajoPorHacerActual;

    /// <summary>
    /// Tiempo en segundos entre ejecuciones de la actividad actual
    /// </summary>
    public float tiempoEntreActualizacionesActividad;
    #endregion Impulsos

    #region Comunicacion
    public Comunicador comunicadorPrincipal;
    #endregion Comunicacion

    #region Movimiento

    /// <summary>
    /// Tiempo en segundos que tarda en dar una vuelta el humano
    /// </summary>
    public float timeToRotate = 1f;

    /// <summary>
    /// Velocidad con la que anda el Humano
    /// </summary>
    public float walkingSpeed = 3f;

    /// <summary>
    /// Indica si el Humano está dando la vuelta.
    /// </summary>
    private bool girando = false;
    #endregion Movimiento

    #region Visión y sentidos
    /// <summary>
    /// Area de visión del Humano hacia la izquierda (x negativos)
    /// </summary>
    public Collider2D visionIzquierda;

    /// <summary>
    /// Area de visión del Humano hacia la derecha (x positivos)
    /// </summary>
    public Collider2D visionDerecha;

    #endregion Visión y sentidos

    #region Referencias a componentes

    private SpriteRenderer mainSpriteRenderer;      

    private Transform _transform;

    #endregion Referencias a componentes

    #endregion Campos y Propiedades

    #region Funciones de Unity

    /// <summary>
    /// Inicializa las referencias a componentes.
    /// </summary>
    public override void Start()
    {
        base.Start();
        _transform = transform;
        mainSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }
    
    /// <summary>
    /// Se llama cuando algo entra en el campo de visión del Humano.
    /// </summary>
    /// <param name="other">Collider2D que ha entrado en el campo de visión.</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        Investigable investigable = other.gameObject.GetComponent<Investigable>();
        if (investigable != null)  //primero comprobamos que es un Investigable
        {
            //Primero comprobar si el Investigable le asusta
            if (ComprobarSusto(investigable))
            {
                Comenta(investigable, Intereses.Miedo);
            }
            /*
            //Segundo, comprobar si despierta su curiosidad
            if (investigable.Misterio >= Curiosidad)
            {
                ObjetosConocidosInteresantes.Add(investigable);
                Comenta(investigable, Intereses.Misterio);
            }

            //Tercero, comprobar si es interesante como para dejar lo que está haciendo
            if (investigable.Interes >= Ocio)
            {
                ObjetosConocidosInteresantes.Add(investigable);
                Comenta(investigable, Intereses.Ocio);
            }         
            */
        }
    }

    #endregion Funciones de Unity

    #region Funciones de Movimiento
    
    /// <summary>
    /// Da la vuelta en x al humano, tardando timeToRotate segundos.
    /// </summary>
    private void DarLaVuelta()
    {
        if (!girando)
            StartCoroutine(DaLaVuelta());
    }
    
    /// <summary>
    /// Da la vuelta en x al humano, tardando timeToRotate segundos. Corutina.
    /// </summary>
    /// <returns></returns>
    private IEnumerator DaLaVuelta()
    {
        if (!girando)
        {
            girando = true;
            //Primero se gira desde los 0º hasta los 90º
            float i = 0f;
            float velocidad = 2 / timeToRotate;
            while (i < 1)
            {
                i += Time.deltaTime * velocidad;
                Vector3 rotation = _transform.rotation.eulerAngles;
                float newY = Mathf.Lerp(rotation.y, 90, i);
                Vector3 newRotation = new Vector3(rotation.x, newY, rotation.z);
                _transform.rotation = Quaternion.Euler(newRotation);
                yield return null;
            }
            Vector3 rotationFinal = _transform.rotation.eulerAngles;
            _transform.rotation = Quaternion.Euler(new Vector3(rotationFinal.x, 90, rotationFinal.z));

            //Se invierte el sprite
            mainSpriteRenderer.flipX = !mainSpriteRenderer.flipX;

            //Se cambia el campo de visión activo 
            visionIzquierda.enabled = !mainSpriteRenderer.flipX;
            visionDerecha.enabled = mainSpriteRenderer.flipX;

            //Ahora se rota desde los 90º hasta los 0º
            i = 0f;
            while (i < 1)
            {
                i += Time.deltaTime * velocidad;
                Vector3 rotation = _transform.rotation.eulerAngles;
                float newY = Mathf.Lerp(rotation.y, 0, i);
                Vector3 newRotation = new Vector3(rotation.x, newY, rotation.z);
                _transform.rotation = Quaternion.Euler(newRotation);
                yield return null;
            }
            rotationFinal = _transform.rotation.eulerAngles;
            _transform.rotation = Quaternion.Euler(new Vector3(rotationFinal.x, 0, rotationFinal.z));
            girando = false;
        }
    }

    /// <summary>
    /// inicia la corutina de movimiento para ir al destino actual.
    /// </summary>
    public void IrDestino()
    {
        Vector3 destino = destinoActual.PuntoDeInteraccion.position;
        StartCoroutine(MoverseADestino(destino));
    }

    /// <summary>
    /// Corutina para moverse hasta el destino actual
    /// </summary>
    /// <param name="destino">Punto de destino</param>
    private IEnumerator MoverseADestino(Vector3 destino)
    {
        MirarDestinoActual();
        while (girando)
        {
            yield return new WaitUntil(() => !girando);
        }
        animator.SetBool("Andando", true);
        float direccion = (destino.x > transform.position.x) ? 1 : -1;
        while (Mathf.Abs(destino.x - transform.position.x) >= 0.1f)
        {
            float newX = transform.position.x + direccion * Time.deltaTime * walkingSpeed;
            transform.position = new Vector3(newX, transform.position.y, transform.position.z);
            yield return null;
        }
        transform.position = new Vector3(destino.x, transform.position.y, transform.position.z);
        animator.SetBool("Andando", false);
        animator.SetTrigger("LlegadoDestino");
    }

    /// <summary>
    /// Comprueba si tiene que girar para mirar al destino actual y, en ese caso, inicia la corutina de giro.
    /// </summary>
    public void MirarDestinoActual()
    {
        if (destinoActual.transform.position.x >= _transform.position.x)
        {
            if (!mainSpriteRenderer.flipX)
            {
                StartCoroutine(DaLaVuelta());
            }
        }
        else
        {
            if (mainSpriteRenderer.flipX)
            {
                StartCoroutine(DaLaVuelta());
            }
        }
    }

    #endregion Funciones de Movimiento

    #region Funciones de Impulsos

    public bool QuiereOcio()
    {
        float probabilidadBase = Ocio - Aguante;
        probabilidadBase += Random.Range(-20, 20);

        return Random.Range(0, 100) <= probabilidadBase;
    }

    /// <summary>
    /// Decide cuál de los trabajos que tiene pendientes le es más productivo y lo marca como DestinoActual.
    /// Avisa al Animator con TrabajoEncontrado o NoHayTrabajo.
    /// </summary>
    public void BuscarTrabajo()
    {
        TrabajoPorHacer objetivo = LugaresDeTrabajoConocidos[0];
        int mejorTrabajoEncontrado = objetivo.Productividad();

        foreach (TrabajoPorHacer oc in LugaresDeTrabajoConocidos)
        {
            int productividad = oc.Productividad();
            if (productividad >= mejorTrabajoEncontrado)
            {
                mejorTrabajoEncontrado = productividad;
                objetivo = oc;
            }
        }

        if (mejorTrabajoEncontrado > 0)
        {
            destinoActual = objetivo.lugar;
            trabajoPorHacerActual = objetivo;
            Comenta(destinoActual, Intereses.Trabajo);
            animator.SetBool("TrabajoEncontrado", true);
        }
        else
        {
            destinoActual = null;
            animator.SetBool("NoHayTrabajo", true);
        }
    }


    /// <summary>
    /// Decide entre los lugares de ocio que conoce al azar.
    /// Avisa al Animator con OcioEncontrado.
    /// </summary>
    public void BuscarOcio()
    {
        int lugarElegido = Mathf.FloorToInt(Random.Range(0, LugaresDeOcioConocidos.Length));
        Investigable objetivo = LugaresDeOcioConocidos[lugarElegido];

        destinoActual = objetivo;
        Comenta(destinoActual, Intereses.Ocio);
        animator.SetTrigger("OcioEncontrado");
    }

    public void EmpezarTrabajoEnDestinoActual()
    {
        trabajoActual = destinoActual;
    }

    public void EmpezarOcioEnDestinoActual()
    {
        ocioActual = destinoActual;
    }

    public void Trabajar()
    {
        if (Time.time >= ultimoTiempoActualizacionActividad + tiempoEntreActualizacionesActividad)
        {
            int trabajoAvanzado = Mathf.FloorToInt(Aguante * trabajoPorHacerActual.lugar.Trabajo * 0.01f);
            trabajoPorHacerActual.cantidad -= trabajoAvanzado;
            Aguante -= trabajoAvanzado;
            foreach (TrabajoPorHacer tph in LugaresDeTrabajoConocidos)
            {
                if (!tph.Equals(trabajoPorHacerActual))
                {
                    tph.cantidad += Mathf.FloorToInt(Random.Range(1, 10));
                }
            }

            ultimoTiempoActualizacionActividad = Time.time;

            if (Aguante < Ocio)
            {
                animator.SetTrigger("CansadoDeTrabajar");
            }
            else if (trabajoPorHacerActual.cantidad <= 0)
            {
                trabajoPorHacerActual.cantidad = Mathf.FloorToInt(Random.Range(0, 100));
                animator.SetTrigger("TrabajoActualTerminado");
            }
            else
            {
                Comenta(trabajoActual, Intereses.Trabajo);
            }
        }
    }

    public void Divertirse()
    {
        if (Time.time >= ultimoTiempoActualizacionActividad + tiempoEntreActualizacionesActividad)
        {
            int ocioRealizado = Mathf.FloorToInt(Ocio * destinoActual.Ocio * 0.01f);

            Aguante += ocioRealizado;

            foreach (TrabajoPorHacer tph in LugaresDeTrabajoConocidos)
            {
                if (!tph.Equals(trabajoPorHacerActual))
                {
                    tph.cantidad += Mathf.FloorToInt(Random.Range(1, 5));
                }
            }

            ultimoTiempoActualizacionActividad = Time.time;

            if (QuiereOcio())
            {
                Comenta(ocioActual, Intereses.Ocio);
            }
            else
            {
                animator.SetTrigger("OcioTerminado");
            }
        }
    }

    #endregion Funciones de Impulsos

    #region Funciones de comunicación

    /// <summary>
    /// Comenta visiblemente el objeto indicado en el interés indicado
    /// </summary>
    /// <param name="objeto">El objeto Investigable que ha despertado interés</param>
    /// <param name="campo">El campo de Interés</param>
    public void Comenta(Investigable objeto, Intereses campo)
    {
        MensajeComunicado mensaje = new MensajeComunicado(campo, objeto);
        comunicadorPrincipal.Comunica(mensaje);
    }

    #endregion Funciones de comunicación





 
}
