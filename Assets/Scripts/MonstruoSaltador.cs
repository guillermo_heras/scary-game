﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MonstruoSaltador : Player {
 /*   //TODO refactor this class to only add jumping logic
	public float walkSpeed = 2f;
	public float jumpStrength = 2f;
	public float gravityForce = 1f;
	public float airHandling = 0.5f;
	public LayerMask mask;

	public float jumpingMaxForceTime = 0.3f;

	public Transform middleFront;

	protected Animator animator;
	protected Rigidbody2D rb2D;
	protected Transform _transform;
	protected BoxCollider2D _collider;

	protected bool isJumping = false;
	protected float jumpStartingTime;
	protected float jumpingForceTime;

	protected Quaternion forwardRotation;
	protected Quaternion backwardRotation;

	void Start () {
		animator = GetComponent<Animator> ();
		rb2D = GetComponent <Rigidbody2D>();
		_transform = transform;
		_collider = GetComponent<BoxCollider2D>();

		forwardRotation = Quaternion.Euler (new Vector3(0,0,0));
		backwardRotation = Quaternion.Euler (new Vector3(0,180,0));
	}

	void Update () {
		int horizontal = 0;
		int vertical = 0;
		float xMove = 0f;
		float yMove = 0f;

		horizontal = (int)Input.GetAxisRaw ("Horizontal");
		vertical = (int)Input.GetAxisRaw ("Vertical");

		xMove = horizontal * walkSpeed * Time.deltaTime;

		if ((vertical > 0) && (!isJumping)) {
			isJumping = true;
			jumpStartingTime = Time.time;
			jumpingForceTime = 0f;
			xMove *= airHandling;
		}

		if (isJumping) {
			yMove -= gravityForce * Time.deltaTime;

			if ((vertical > 0) && (jumpingForceTime <= jumpingMaxForceTime)) {
				jumpingForceTime += Time.deltaTime;
				yMove += vertical * jumpStrength * Time.deltaTime / jumpingForceTime;
			}
		}

		if (horizontal > 0) {
			_transform.rotation = forwardRotation;
		} else if (horizontal < 0) {
			_transform.rotation = backwardRotation;
		}

		Vector2 movement = new Vector2(xMove,yMove);

		RaycastHit2D rayCast = Physics2D.Raycast(middleFront.position,movement,movement.magnitude,mask);

		if (rayCast.collider == null) {
			Vector2 newPosition = new Vector2(_transform.position.x + xMove, _transform.position.y + yMove);
			rb2D.MovePosition (newPosition);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Floor")
			isJumping = false;
	}
	*/

}
