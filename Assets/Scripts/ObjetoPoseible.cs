﻿using UnityEngine;
using System.Collections;

public class ObjetoPoseible : MonoBehaviour {

    public Color MainColor;


    protected Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {


    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            _animator.SetBool("PlayerNear", true);
            other.gameObject.GetComponent<Player>().AddObjetoPoseibleCercano(this);
        }
    }

    public void SetPlayerFocus(bool playerFocus)
    {
        _animator.SetBool("PlayerFocus", playerFocus);
    }
    

    public void SetPossession(bool poseido)
    {
        _animator.SetBool("Possessed",poseido);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            _animator.SetBool("PlayerNear", false);
            other.gameObject.GetComponent<Player>().RemoveObjetoPoseibleCercano(this);
        }
    }

    public void EfectoPosesion()
    {
        _animator.SetTrigger("EfectoPoseer");
    }
}
