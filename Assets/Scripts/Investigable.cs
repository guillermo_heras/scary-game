﻿using UnityEngine;
using System.Collections;

public class Investigable : MonoBehaviour {

    [Range(0,100)]
    public int Misterio, Interes, Horror, Trabajo, Ocio;

    public string nombre;

    [SerializeField]
    private Transform puntoDeInteraccion;
    public Transform PuntoDeInteraccion
    {
        get
        {
            return puntoDeInteraccion;
        }

        set
        {
            puntoDeInteraccion = value;
        }
    }

    [SerializeField]
    private Transform puntoAMirarParaInteractuar;
    public Transform PuntoAMirarParaInteractuar
    {
        get
        {
            return puntoAMirarParaInteractuar;
        }

        set
        {
            puntoAMirarParaInteractuar = value;
        }
    }

    public Sprite GetSprite()
    {
        SpriteRenderer spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            return spriteRenderer.sprite;
        }
        else
            return null;
    }


}
