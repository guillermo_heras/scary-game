﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(Humano))]
public class HumanoEditor : AsustableEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Humano humano = (Humano)target;

        if (humano.DestinoActual)
            EditorGUILayout.LabelField("Destino actual: " + humano.DestinoActual.name);
        else
            EditorGUILayout.LabelField("Destino actual: Ninguno");

        if (humano.TrabajoActual)
            EditorGUILayout.LabelField("Trabajo actual: " + humano.TrabajoActual.name);
        else
            EditorGUILayout.LabelField("Trabajo actual: Ninguno");

    }
}
