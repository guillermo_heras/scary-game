﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Player))]
public class PlayerEditor  : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Player player = (Player)target;

        EditorGUILayout.LabelField("Poder de miedo");
        player.PoderDeMiedo = EditorGUILayout.IntSlider(player.PoderDeMiedo, 0, 100);

        

    }
}
