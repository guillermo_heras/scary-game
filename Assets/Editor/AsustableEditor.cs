﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(Asustable), true)]
public class AsustableEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Asustable asustable = (Asustable)target;

        asustable.NivelDeMiedo = EditorGUILayout.IntSlider("Nivel de Miedo", asustable.NivelDeMiedo, 0, 100);
        EditorGUILayout.LabelField("Nivel de Horror: " + asustable.NivelDeHorror);
        EditorGUILayout.LabelField("Valentía: " + asustable.Valentia);
    }
}
